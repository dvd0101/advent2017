#include <iostream>
#include <regex>
#include <numeric>
#include <unordered_map>

enum class dir {
	left,
	right
};

std::ostream& operator<<(std::ostream& out, dir d) {
	return out << (d == dir::left ? "left" : "right");
}

struct state {
	struct action {
		int value;
		dir direction;
		char state;
	};

	char name;
	action actions[2];
};

std::ostream& operator<<(std::ostream& out, const state::action& s) {
	return out << "value=" << s.value << " dir=" << s.direction << " state=" << s.state;
}

std::ostream& operator<<(std::ostream& out, const state& s) {
	return out << "state " << s.name << " 0 -> " << s.actions[0] << " 1 -> " << s.actions[1];
}

struct blueprint {
	char start;
	int checksum_steps;
	std::unordered_map<char, state> states;
};

std::istream& parse(std::istream& input, state& s) {
	static const std::regex state_name("state (.)");
	static const std::regex write_value("value (\\d)");
	static const std::regex move_dir("Move one slot to the ([^.]+)");

	using namespace std;
	smatch m;
	for (string line; getline(input, line);) {
		if (regex_search(line, m, state_name)) {
			s.name = static_cast<string>(m[1])[0];
			break;
		}
	}

	if (input) {
		string line;
		for (int i = 0; i < 2; i++) {
			getline(input, line);

			getline(input, line);
			regex_search(line, m, write_value);
			s.actions[i].value = stoi(static_cast<string>(m[1]));

			getline(input, line);
			regex_search(line, m, move_dir);
			s.actions[i].direction = m[1] == "left" ? dir::left : dir::right;

			getline(input, line);
			regex_search(line, m, state_name);
			s.actions[i].state = static_cast<string>(m[1])[0];
		}
	}

	return input;
}

blueprint parse(std::istream& input) {
	static const std::regex start("Begin in state (.)");
	static const std::regex steps("checksum after (\\d+)");

	using namespace std;
	blueprint b;
	string line;
	smatch m;

	getline(input, line);
	regex_search(line, m, start);
	b.start = static_cast<string>(m[1])[0];

	getline(input, line);
	regex_search(line, m, steps);
	b.checksum_steps = stoi(m[1]);

	state s;
	while (parse(input, s)) {
		b.states[s.name] = s;
	}
	return b;
}

struct machine {
	machine(const blueprint& b) : cur_state{b.start}, cur_pos{0}, states{b.states} {}

	void execute() {
		auto& s = states[cur_state];
		int& value = tape[cur_pos];
		int ix = value;
		value = s.actions[ix].value;
		cur_pos += s.actions[ix].direction == dir::left ? -1 : 1;
		cur_state = s.actions[ix].state;
	}

	int checksum() const {
		return std::accumulate(
			tape.begin(),
			tape.end(),
			0,
			[](int t, const auto& v) { return t + v.second; });
	}

	char cur_state;
	int cur_pos;
	std::unordered_map<char, state> states;
	std::unordered_map<int, int> tape;
};

int main() {
	auto b = parse(std::cin);
	std::cout << b.start << " " << b.checksum_steps << "\n";
	for (auto& [name, s] : b.states) {
		std::cout << name << " / " << s << "\n";
	}

	machine m(b);
	while (b.checksum_steps-- > 0) {
		m.execute();
	}
	std::cout << m.checksum() << "\n";
}
