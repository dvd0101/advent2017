#include <iostream>
#include <cassert>
#include <string>
#include <regex>
#include <tuple>
#include <vector>
#include <unordered_map>
#include <optional>

struct program {
	std::string name;
	int weight;
};

const std::regex parser("(\\w+) \\((\\d+)\\)( -> (.*))?");
const std::regex children_parser("\\w+");

std::tuple<program, std::vector<std::string>> parse(const std::string& line) {
	using namespace std;

	smatch match;
	bool r = regex_match(line, match, parser);
	assert(r);

	program p{match[1], stoi(match[2])};
	vector<string> children;

	if (match[4].length() != 0) {
		string deps = match[4];
		auto last = remove(begin(deps), end(deps), ' ');
		copy(
			sregex_token_iterator(begin(deps), last, children_parser),
			sregex_token_iterator(),
			back_inserter(children)
		);
	}
	return {p, std::move(children)};
}

struct node {
	program prg;
	std::string parent;
	int total_weight;
};

struct db {
	std::unordered_map<std::string, node> nodes;
	std::unordered_map<std::string, std::vector<std::string>> index;

	std::string root() const {
		using namespace std;
		return find_if(begin(nodes), end(nodes), [](auto& v) { return v.second.parent == ""; })->first;
	}

	void calculate_weights() {
		calculate_weight(root());
	}

	int calculate_weight(const std::string& id) {
		auto& node = nodes.at(id);
		const auto& prg = node.prg;
		int w = prg.weight;
		for (auto& child : index[prg.name]) {
			w += calculate_weight(child);
		}
		node.total_weight = w;
		return w;
	}

};


int find_wrong_program(const db& data, const std::string& root) {
	using namespace std;

	auto& children = data.index.at(root);
	vector<int> weights;
	transform(
		begin(children),
		end(children),
		back_inserter(weights),
		[&](auto& id) { return data.nodes.at(id).total_weight; });

	assert(weights.size() >= 3);
	auto [min_pos, max_pos] = minmax_element(begin(weights), end(weights));
	if (*min_pos == *max_pos) {
		return 0;
	}

	auto next_pos = next(min_pos);
	while (next_pos == max_pos) {
		next_pos = next(next_pos);
	}


	std::string wrong;
	int offset;
	if (*min_pos == *next_pos) {
		wrong = children[distance(begin(weights), max_pos)];
		offset = *min_pos - *max_pos;
		cout << root << " -> " << wrong << " " << *max_pos << " (" << *min_pos << ")" << "\n";
	} else {
		wrong = children[distance(begin(weights), min_pos)];
		offset = *max_pos - *min_pos;
		cout << root << " -> " << wrong << " " << *min_pos << " (" << *max_pos << ")" << "\n";
	}

	auto child = find_wrong_program(data, wrong);
	return child ? child : data.nodes.at(wrong).prg.weight + offset;
}

int find_wrong_program(const db& data) {
	return find_wrong_program(data, data.root());
}

int main() {
	using namespace std;

	db dataset;

	auto& nodes = dataset.nodes;
	auto& index = dataset.index;
	for (string line; getline(cin, line); ) {
        auto [p, children] = parse(line);
        // if the entry does not exists the parent is default constructed to ""
        nodes[p.name].prg = p;
        index[p.name] = children;
        for (auto& child : children) {
			auto pos = nodes.find(child);
			if (pos != end(nodes)) {
				pos->second.parent = p.name;
			} else {
				nodes[child] = node{program{child, 0}, p.name, 0};
			}
		}
    }

	dataset.calculate_weights();
	cout << find_wrong_program(dataset) << "\n";
}
