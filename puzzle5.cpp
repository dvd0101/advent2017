#include <iostream>
#include <algorithm>

const int key = 325489;

constexpr int pow(int n) { return n * n; }

constexpr int level(int ord) {
	return 2 * ord + 1;
}

struct ring {
	constexpr ring(int r) : ord{r} {}

	constexpr static ring find(int value) {
		int r = 0;
		while (pow(::level(r)) < value) {
			++r;
		}
		return ring{r};
	}

	constexpr int start() const {
		return ord == 0 ? 1 : (pow(level(ord - 1)) + 1);
	}

	constexpr int end() const {
		return ord == 0 ? 1 : pow(level(ord));
	}

	constexpr int east() const {
		const int offset = ord == 0 ? 0 : ord - 1;
		return start() + offset;
	}

	constexpr int north() const {
		return east() + level(ord) - 1;
	}

	constexpr int west() const {
		return north() + level(ord) - 1;
	}

	constexpr int south() const {
		return west() + level(ord) - 1;
	}

	int ord;
};

constexpr int distance(int value) {
	using namespace std;

	const auto r = ring::find(value);
	const int refs[4] = {
		r.east(),
		r.north(),
		r.west(),
		r.south()
	};
	const int distances[4] = {
		abs(value - refs[0]),
		abs(value - refs[1]),
		abs(value - refs[2]),
		abs(value - refs[3])
	};
	const auto it = distance(begin(distances), min_element(begin(distances), end(distances)));
	return abs(value - refs[it]) + r.ord;
}

int main() {
	using namespace std;

	cout << "r1 " << distance(1) << "\n";
	cout << "r12 " << distance(12) << "\n";
	cout << "r23 " << distance(23) << "\n";
	cout << "r1024 " << distance(1024) << "\n";
	cout << "r " << distance(key) << "\n";
}
