#include <iostream>
#include <string>
#include <vector>

struct csv_whitespace : std::ctype<char> {
    static const mask* make_table() {
        // make a copy of the "C" locale table
        static std::vector<mask> v(classic_table(), classic_table() + table_size);
        v[','] |=  space;  // comma will be classified as whitespace
        return &v[0];
    }
    csv_whitespace(std::size_t refs = 0) : ctype(make_table(), false, refs) {}
};

enum class dir {
	n,
	ne,
	se,
	s,
	sw,
	nw
};

std::istream& operator>>(std::istream& input, dir& d) {
	if (std::string t; input >> t) {
		if (t == "n") {
			d = dir::n;
		} else if (t == "ne") {
			d = dir::ne;
		} else if (t == "se") {
			d = dir::se;
		} else if (t == "s") {
			d = dir::s;
		} else if (t == "sw") {
			d = dir::sw;
		} else if (t == "nw") {
			d = dir::nw;
		}
	}
	return input;
}

struct hex {
	int x = 0;
	int y = 0;

	hex& move(dir d) {
		switch (d) {
			case dir::n:
				y += 2;
				break;
			case dir::ne:
				x++;
				y++;
				break;
			case dir::se:
				x++;
				y--;
				break;
			case dir::s:
				y -= 2;
				break;
			case dir::sw:
				x--;
				y--;
				break;
			case dir::nw:
				x--;
				y++;
				break;
		}
		return *this;
	}

	bool operator==(hex o) const {
		return x == o.x && y == o.y;
	}

	bool operator!=(hex o) const {
		return !(*this == o);
	}
};

int traverse(hex src, hex dst) {
	int steps = 0;
	while (src != dst) {
		dir d;
		if (src.x == dst.x) {
			d = src.y > dst.y ? dir::s : dir::n;
		} else if (src.x > dst.x) {
			d = src.y > dst.y ? dir::sw : dir::nw;
		} else {
			d = src.y > dst.y ? dir::se : dir::ne;
		}
		src.move(d);
		++steps;
	}
	return steps;
}

int main() {
	hex grid;

	dir d;
	const hex origin;
	int distance = 0;
	std::cin.imbue(std::locale(std::cin.getloc(), new csv_whitespace));
	while (std::cin >> d) {
		distance = std::max(distance, traverse(grid.move(d), origin));
	}
	std::cout << distance << "\n";
}
