#include <iostream>
#include <optional>
#include <vector>

struct whitespace : std::ctype<char> {
    static const mask* make_table() {
        static std::vector<mask> v(classic_table(), classic_table() + table_size);
        v[':'] |=  space;
        return &v[0];
    }
    whitespace(std::size_t refs = 0) : ctype(make_table(), false, refs) {}
};

struct layer {
	layer(int r) : range{r} {};

	int pos(int time) const {
		return time % ((range - 1) * 2);
	}

	int range;
};

using firewall = std::vector<std::optional<layer>>;

firewall parse(std::istream& input) {
	firewall fw;

	input.imbue(std::locale(std::cin.getloc(), new whitespace));
	size_t depth, range;
	while (input >> depth >> range) {
		if (depth >= fw.size()) {
			fw.resize(depth + 1);
		}
		fw[depth] = layer(range);
	}

	return fw;
}

int trip_severity(firewall fw) {
	int severity = 0;
	for (size_t depth = 0; depth < fw.size(); ++depth) {
		auto& l = fw[depth];
		if (l && l->pos(depth) == 0) {
			severity += depth * l->range;
		}
	}

	return severity;
}

int main() {
	auto fw = parse(std::cin);
	std::cout << trip_severity(fw) << "\n";
}
