#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>

void relocate(std::vector<int>& memory) {
	using namespace std;

	int cells = 0;
	auto pos = max_element(begin(memory), end(memory));
	swap(cells, *pos);

	auto ix = distance(begin(memory), pos) + 1;
	while (cells--) {
		memory[ix++ % memory.size()]++;
	}
}

int main() {
	using namespace std;

	using memory_banks = vector<int>;
	vector<memory_banks> history;
	memory_banks memory{istream_iterator<int>(cin), istream_iterator<int>()};

	while (find(begin(history), end(history), memory) == end(history)) {
		history.push_back(memory);
		relocate(memory);
	}

	memory_banks reference = memory;
	size_t counter = 0;
	do {
		relocate(memory);
		counter++;
	} while (reference != memory);
	std::cout << counter << "\n";
}
