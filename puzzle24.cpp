#include <cassert>
#include <iostream>
#include <queue>
#include <regex>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using links = std::unordered_map<int, std::vector<int>>;

const std::regex parser("(\\d+) <-> (.*)");
const std::regex endpoints("\\d+");

void parse(const std::string& line, links& output) {
	using namespace std;

	smatch splits;
	bool r = regex_match(begin(line), end(line), splits, parser);
	assert(r);

	vector<int> connections;

	std::string raw = splits[2];
	for_each(
		sregex_token_iterator(begin(raw), end(raw), endpoints),
		sregex_token_iterator(),
		[&connections](const std::string& endpoint) {
			connections.push_back(stoi(endpoint));
		});

	int a = stoi(splits[1]);
	output[a] = std::move(connections);
}

links parse(std::istream& input) {
	using namespace std;

	links output;
	for (string line; getline(input, line); ) {
		parse(line, output);
    }

    return output;
}


int groups(links& conns) {
	int counter = 0;
	while (!conns.empty()) {
		std::queue<int> stack;
		stack.push(conns.begin()->first);
		while(!stack.empty()) {
			const int src = stack.front();
			stack.pop();
			if (!conns.count(src)) {
				continue;
			}
			for (int dst : conns[src]) {
				stack.push(dst);
			}
			conns.erase(src);
		}
		++counter;
	}

	return counter;
}

int main() {
	links connections = parse(std::cin);
	std::cout << groups(connections) << "\n";
}
