#include <iostream>
#include <cassert>
#include <functional>
#include <string>
#include <iterator>
#include <vector>


struct coords {
	int x, y;

	bool operator==(coords c) const {
		return x == c.x && y == c.y;
	}
};

std::ostream& operator<<(std::ostream& out, coords c) {
	return out << "[" << c.x << "," << c.y << "]";
}

struct pattern {
	using storage = std::vector<char>;

	pattern() : size_{0} {}
	pattern(char s, storage d) : size_{s}, dots{std::move(d)} {}

	char operator[](coords c) const {
		return dots.at(c.y * size_ + c.x);
	}

	bool operator==(const pattern& o) const {
		return dots == o.dots;
	}

	int one_bits() const {
		return std::count(dots.begin(), dots.end(), 1);
	}

	char size() const { return size_; }

	char size_;
	std::vector<char> dots;
};

class dots_iterator;
using map_f = std::function<coords(coords)>;

map_f pipe(map_f a, map_f b) {
	return [=](coords c) { return b(a(c)); };
}

class pattern_view {
	public:
		pattern_view(const pattern& p) : pattern_view(p, [](coords c) { return c; }) {}
		pattern_view(const pattern& p, map_f f) : pt{&p}, map{f} {}
		pattern_view(const pattern_view& p, map_f f) : pt{&(p.data())}, map{pipe(p.map, f)} {}

		const pattern& data() const {
			return *pt;
		}

		dots_iterator begin() const;
		dots_iterator end() const;

		char size() const {
			return pt->size();
		}

		char operator[](coords c) const {
			// std::cout << "[]v " << c << " -> " << map(c) << "\n";
			return (*pt)[map(c)];
		}

		bool operator==(pattern_view o) const;

	public:
		template <typename T>
		static pattern_view rotate_cw(const T& p) {
			char size = p.size();
			auto f = [size](coords c) { return coords{c.y, size - 1 - c.x}; };
			return pattern_view(p, f);
		}

		template <typename T>
		static pattern_view rotate_ccw(const T& p) {
			char size = p.size();
			auto f = [size](coords c) { return coords{size - 1 - c.y, c.x}; };
			return pattern_view(p, f);
		}

		template <typename T>
		static pattern_view flip_x(const T& p) {
			char size = p.size();
			auto f = [size](coords c) { return coords{size - 1 - c.x, c.y}; };
			return pattern_view(p, f);
		}

		template <typename T>
		static pattern_view flip_y(const T& p) {
			char size = p.size();
			auto f = [size](coords c) { return coords{c.x, size - 1 - c.y}; };
			return pattern_view(p, f);
		}


	private:
		const pattern* pt;
		map_f map;
};

class dots_iterator : public std::iterator<std::forward_iterator_tag, char> {
	public:
		dots_iterator() : p{nullptr} {}
		dots_iterator(const pattern_view& pt) : p{&pt}, c{} {}

		bool operator==(dots_iterator other) const {
			return (p == nullptr && other.p == nullptr) ||
				(p == other.p && c == other.c);
		}

		bool operator!=(dots_iterator other) const {
			return !(*this == other);
		}

		dots_iterator& operator++() {
			++c.x;
			if (c.x >= p->size()) {
				c.x = 0;
				++c.y;
			}
			if (c.y >= p->size()) {
				p = nullptr;
			}
			return *this;
		}

		dots_iterator operator++(int) {
			auto r = *this;
			++(*this);
			return r;
		}

		char operator*() const {
			return (*p)[c];
		}

	private:
		const pattern_view* p;
		coords c;
};

dots_iterator pattern_view::begin() const { return dots_iterator(*this); }
dots_iterator pattern_view::end() const { return dots_iterator(); }
bool pattern_view::operator==(pattern_view o) const {
	return std::equal(begin(), end(), o.begin());
}

void copy(std::ostream& out, pattern_view view) {
	char size = view.size();
	int ix = 0;
	for (auto c : view) {
		if (ix > 0 && ix % size == 0) out << '/';
		out << (c ? '#' : '.');
		++ix;
	}
}

bool equivalent(const pattern& a, const pattern& b) {
	using namespace std;
	return a.size() == b.size() && (
		a == b ||
		pattern_view::flip_x(a) == b ||
		pattern_view::flip_y(a) == b ||
		pattern_view::rotate_cw(a) == b ||
		pattern_view::flip_x(pattern_view::rotate_cw(a)) == b ||
		pattern_view::flip_y(pattern_view::rotate_cw(a)) == b ||
		pattern_view::flip_x(pattern_view::rotate_ccw(a)) == b ||
		pattern_view::flip_y(pattern_view::rotate_ccw(a)) == b
	);
}


std::ostream& operator<<(std::ostream& out, const pattern& p) {
	copy(out, p);
	return out;
}

std::ostream& operator<<(std::ostream& out, const pattern_view& p) {
	copy(out, p);
	return out;
}

std::istream& operator>>(std::istream& input, pattern& p) {
	char c;
	std::vector<char> bits;
	while (input) {
		input >> c;
		if (c == '/')
			break;
		bits.push_back(c == '#' ? 1 : 0);
	}
	if (!input) {
		return input;
	}
	assert(bits.size() > 0);
	int size = bits.size();
	int remainder = bits.size() - 1;
	while (remainder-- > 0) {
		for (int ix = 0; ix < size; ++ix) {
			input >> c;
			bits.push_back(c == '#' ? 1 : 0);
		}
		input.ignore(1);
	}
	p = pattern{size, bits};
	return input;
}

struct rule {
	pattern reference;
	pattern output;
};

std::vector<rule> parse(std::istream& input) {
	using namespace std;
	vector<rule> output;

	pattern rule;
	pattern result;
	while ((input >> rule).ignore(4, '>') >> result) {
		output.push_back({rule, result});
	}
	return output;
}

struct canvas {
	void draw(const std::vector<rule>& drawbook) {
		assert(size % 2 == 0 || size % 3 == 0);

		std::vector<pattern> new_patterns;
		int ix = 0;
		for (auto& p : extract()) {
			bool found = false;
			for (auto& r : drawbook) {
				if (equivalent(p, r.reference)) {
					new_patterns.push_back(r.output);
					ix++;
					found = true;
					break;
				}
			}
			assert(found);
		}
		merge(new_patterns);
	}

	int pattern_step() const {
		return (size % 2 == 0) ? 2 : 3;
	}

	int patterns_per_row() const {
		return size / pattern_step();
	}

	std::vector<pattern> extract() const {
		std::vector<pattern> out;

		int step = pattern_step();
		int counter = patterns_per_row() * patterns_per_row();
		std::cout << "extract " << counter << " " << patterns_per_row() << "\n";
		for (int ic = 0; ic < counter; ++ic) {
			int x = (ic * step) % size;
			int y = (ic * step) / size * step;
			std::vector<char> bits;
			for (int iy = y; iy < y + step; ++iy) {
				for (int ix = x; ix < x + step; ++ix) {
					bits.push_back(pixels.at(iy * size + ix));
				}
			}
			out.push_back(pattern{step, bits});
		}
		return out;
	}

	void merge(const std::vector<pattern>& patterns) {
		int row_size = patterns_per_row();
		int pattern_size = patterns[0].size();
		std::cout << "merge " << patterns.size() << " " << pattern_size << "x" << pattern_size << "\n";

		size = row_size * pattern_size;
		pixels.resize(size * size);
		for (size_t ic = 0; ic < patterns.size(); ++ic) {
			int crow = ic / row_size * pattern_size;
			int ccol = ic % row_size * pattern_size;
			for (int y = 0; y < pattern_size; ++y) {
				for (int x = 0; x < pattern_size; ++x) {
					pixels[(crow + y) * size + ccol + x] = patterns[ic][coords{x, y}];
				}
			}
		}
	}

	int count() {
		return std::count(pixels.begin(), pixels.end(), 1);
	}

	int size;
	std::vector<char> pixels;
};

int main() {
	auto drawbook = parse(std::cin);
 	canvas paint{3, {0,1,0,0,0,1,1,1,1}};
 	int steps = 18;
 	while (steps-- > 0) {
		paint.draw(drawbook);
		std::cout << pattern{paint.size, paint.pixels} << "\n";
	}
	std::cout << paint.count() << "\n";
}
