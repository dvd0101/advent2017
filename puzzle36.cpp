#include <iostream>
#include <functional>
#include <map>
#include <variant>
#include <queue>
#include <vector>

using registers = std::map<char, int64_t>;
using io = std::queue<int64_t>;

struct blocked : std::runtime_error {
	blocked() : runtime_error{""} {};
};

class operand {
	public:
		operand(char c) : r{c} {}
		operand(int c) : r{'.'}, v{c} {}

		int operator()(registers& regs) const {
			return r != '.' ? regs[r] : v;
		}

	private:
		char r;
		int v;
};

struct snd {
	char reg;

	void execute(registers& regs, int&, io&, io& out) const {
		out.push(regs[reg]);
	}
};

struct set {
	char reg;
	operand value;

	void execute(registers& regs, int&, io&, io&) const {
		regs[reg] = value(regs);
	}
};

struct add {
	char reg;
	operand value;

	void execute(registers& regs, int&, io&, io&) const {
		regs[reg] += value(regs);
	}
};

struct mul {
	char reg;
	operand value;

	void execute(registers& regs, int&, io&, io&) const {
		regs[reg] *= value(regs);
	}
};

struct mod {
	char reg;
	operand value;

	void execute(registers& regs, int&, io&, io&) const {
		regs[reg] = regs[reg] % value(regs);
	}
};

struct rcv {
	char reg;

	void execute(registers& regs, int&, io& in, io&) const {
		if (in.empty())
			throw blocked{};
		regs[reg] = in.front();
		in.pop();
	}
};

struct jgz {
	char reg;
	operand value;

	void execute(registers& regs, int& pc, io&, io&) const {
		if (reg == '1' || regs[reg] > 0)
			pc += value(regs);
	}
};


using ins = std::variant<snd, set, add, mul, mod, rcv, jgz>;

using one_op = std::map<std::string, std::function<ins(char)>>;
using two_op = std::map<std::string, std::function<ins(char, operand)>>;

one_op one_arg_op{
	{"snd", [](char c) { return snd{c}; }},
	{"rcv", [](char c) { return rcv{c}; }},
};

two_op two_args_op{
	{"set", [](char c, operand v) { return set{c, v}; }},
	{"add", [](char c, operand v) { return add{c, v}; }},
	{"mul", [](char c, operand v) { return mul{c, v}; }},
	{"mod", [](char c, operand v) { return mod{c, v}; }},
	{"jgz", [](char c, operand v) { return jgz{c, v}; }},
};

std::vector<ins> parse(std::istream& input) {
	std::vector<ins> output;

	std::string op;
	char r;
	std::string v;
	while(input >> op >> r) {
		if (one_arg_op.count(op)) {
			output.push_back(one_arg_op[op](r));
		} else {
			input >> v;
			auto& f = two_args_op.at(op);
			if (v[0] >= 'a' && v[0] <= 'z') {
				output.push_back(f(r, v[0]));
			} else {
				output.push_back(f(r, std::stoi(v)));
			}
		}
	}
	return output;
}

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

void execute(const ins& i, registers& regs, int& pc, io& in, io& out) {
	std::visit(overloaded{
		[&](auto& i) { i.execute(regs, pc, in, out); }
	}, i);
}

bool execute(const std::vector<ins> prg, registers& regs, int& pc, io& in, io& out) {
	while (pc < prg.size()) {
		int s = pc;
		try {
			execute(prg[pc], regs, pc, in, out);
		} catch(blocked) {
			return false;
		}
		if (pc == s) pc++;
	}
	return true;
}

int execute(const std::vector<ins>& prg1, const std::vector<ins>& prg2) {
	registers regs1;
	regs1['p'] = 0;
	registers regs2;
	regs2['p'] = 1;
	int pc1 = 0;
	int pc2 = 0;
	io in1;
	io in2;

	int sent = 0;

	auto run1 = [&]() { return execute(prg1, regs1, pc1, in1, in2); };
	auto run2 = [&]() { return execute(prg2, regs2, pc2, in2, in1); };

	bool finished = false;
	while (!finished) {
		finished = run1();
		if (!finished) {
			run2();
			sent += in1.size();
			finished = in1.empty() && in2.empty();
		}
	}
	return sent;
}

int main() {
	auto prg = parse(std::cin);
	std::cout << execute(prg, prg) << "\n";
}
