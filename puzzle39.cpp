#include <algorithm>
#include <iostream>
#include <vector>

struct whitespace : std::ctype<char> {
    static const mask* make_table() {
        static std::vector<mask> v(classic_table(), classic_table() + table_size);
        v['p'] |= space;
        v['v'] |= space;
        v['a'] |= space;
        v[','] |= space;
        v['<'] |= space;
        v['>'] |= space;
        v['='] |= space;
        return &v[0];
    }
    whitespace(std::size_t refs = 0) : ctype(make_table(), false, refs) {}
};

struct vector {
	int x, y, z;

	vector operator+(const vector& o) {
		return {x + o.x, y + o.y, z + o.z};
	}
};

struct particle {
	vector pos;
	vector v;
	vector a;

	void run() {
		v = v + a;
		pos = pos + v;
	}

	int distance() const {
		return std::abs(pos.x) + std::abs(pos.y) + std::abs(pos.z);
	}
};

std::vector<particle> parse(std::istream& input) {
	using namespace std;
	std::vector<particle> output;

	input.imbue(locale(input.getloc(), new whitespace));
	int a, b, c, d, e, f, g, h, i;
	while (input >> a >> b >> c >> d >> e >> f >> g >> h >> i) {
		output.push_back({
			{a, b, c},
			{d, e, f},
			{g, h, i},
		});
	}
	return output;
}

int main() {
	auto particles = parse(std::cin);
	int counter = 200000;
	while (counter-- > 0) {
		for (auto& p : particles)
			p.run();
	}
	auto it = std::min_element(particles.begin(), particles.end(), [](auto& a, auto& b) { return a.distance() < b.distance(); });
	std::cout << std::distance(particles.begin(), it) << "\n";
}
