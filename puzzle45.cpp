#include <cassert>
#include <iostream>
#include <functional>
#include <map>
#include <variant>
#include <vector>

using registers = std::array<int64_t, 8>;

using operand = std::variant<char, int64_t>;

std::istream& operator>>(std::istream& input, operand& i) {
	std::string v;
	input >> v;
	if (input) {
		if (v.size() == 1 && v[0] >= 'a' && v[0] <= 'h') {
			i = static_cast<char>(v[0] - 'a');
		} else {
			i = static_cast<int64_t>(std::stoi(v));
		}
	}
	return input;
}

int64_t lookup(operand o, const registers& regs) {
	if (auto ptr = std::get_if<char>(&o)) {
		return regs[*ptr];
	}
	return std::get<int64_t>(o);
}

enum opcode {
	set,
	sub,
	mul,
	jnz
};

std::ostream& operator<<(std::ostream& output, opcode& i) {
	switch (i) {
		case opcode::set:
			output << "set";
			break;
		case opcode::sub:
			output << "sub";
			break;
		case opcode::mul:
			output << "mul";
			break;
		case opcode::jnz:
			output << "jnz";
			break;
	}
	return output;
}

std::istream& operator>>(std::istream& input, opcode& i) {
	std::string op;
	input >> op;
	if (input) {
		if (op == "set") {
			i = opcode::set;
		} else if (op == "sub") {
			i = opcode::sub;
		} else if (op == "mul") {
			i = opcode::mul;
		} else if (op == "jnz") {
			i = opcode::jnz;
		} else {
			assert(0);
		}
	}
	return input;
}

struct ins {
	opcode op;
	operand reg;
	operand arg;
};

int muls;

int execute(registers& regs, const ins& i) {
	int offset = 1;
	switch (i.op) {
		case opcode::set:
			regs[std::get<char>(i.reg)] = lookup(i.arg, regs);
			break;
		case opcode::sub:
			regs[std::get<char>(i.reg)] -= lookup(i.arg, regs);
			break;
		case opcode::mul:
			muls++;
			regs[std::get<char>(i.reg)] *= lookup(i.arg, regs);
			break;
		case opcode::jnz:
			if (lookup(i.reg, regs) != 0)
				offset = lookup(i.arg, regs);
			break;
	}
	return offset;
}


std::istream& operator>>(std::istream& input, ins& i) {
	return input >> i.op >> i.reg >> i.arg;
}

using program = std::vector<ins>;

program parse(std::istream& input) {
	std::vector<ins> prg;
	ins op;
	while (input >> op) {
		prg.push_back(op);
	}
	return prg;
}

void execute(registers& regs, const program& prg) {
	int pc = 0;
	muls = 0;
	while (pc < prg.size()) {
		pc += execute(regs, prg[pc]);
	}
}

int main() {
	auto prg = parse(std::cin);
	registers regs{};
	execute(regs, prg);
	std::cout << muls << "\n";
}
