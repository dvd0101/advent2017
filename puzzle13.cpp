#include <iostream>
#include <cassert>
#include <string>
#include <regex>
#include <tuple>
#include <vector>
#include <unordered_map>

struct program {
	std::string name;
	int weight;
};

const std::regex parser("(\\w+) \\((\\d+)\\)( -> (.*))?");
const std::regex children_parser("\\w+");

std::tuple<program, std::vector<std::string>> parse(const std::string& line) {
	using namespace std;

	smatch match;
	bool r = regex_match(line, match, parser);
	assert(r);

	program p{match[1], stoi(match[2])};
	vector<string> children;

	if (match[4].length() != 0) {
		string deps = match[4];
		auto last = remove(begin(deps), end(deps), ' ');
		copy(
			sregex_token_iterator(begin(deps), last, children_parser),
			sregex_token_iterator(),
			back_inserter(children)
		);
	}
	return {p, std::move(children)};
}

struct node {
	program prg;
	std::string parent;
};

int main() {
	std::unordered_map<std::string, node> dataset;

	for (std::string line; std::getline(std::cin, line); ) {
        auto [p, children] = parse(line);
        // if the entry does not exists the parent is default constructed to ""
        dataset[p.name].prg = p;
        for (auto& child : children) {
			auto pos = dataset.find(child);
			if (pos != std::end(dataset)) {
				pos->second.parent = p.name;
			} else {
				dataset[child] = node{program{child, 0}, p.name};
			}
		}
    }

    for (auto& n : dataset) {
		if (n.second.parent == "") {
			std::cout << n.first << "\n";
			break;
		}
	}
}
