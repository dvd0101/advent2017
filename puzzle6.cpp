#include <iostream>
#include <iterator>
#include <unordered_map>

constexpr int pow(int n) { return n * n; }

constexpr int level(int ord) {
	return 2 * ord + 1;
}

struct coord {
	int x, y;

	constexpr bool operator==(coord c) const noexcept {
		return x == c.x && y == c.y;
	}

	constexpr coord operator+(coord c) noexcept {
		return {x + c.x, y + c.y};
	}
};

std::ostream& operator<<(std::ostream& os, coord c) {
	os << "[" << c.x << "," << c.y << "]";
	return os;
}

namespace std {

    template<> struct hash<coord> {
        using argument_type = coord;
        using result_type = std::size_t;

        result_type operator()(argument_type const& coord) const noexcept {
            const std::hash<int> h;
            return h(coord.x) ^ (h(coord.y) << 1);
        }
    };
}

class coords_iterator : public std::iterator<std::forward_iterator_tag, coord> {
	private:
		static constexpr coord up{0, 1};
		static constexpr coord left{-1, 0};
		static constexpr coord bottom{0, -1};
		static constexpr coord right{1, 0};

		static coord start(int ord) {
			return {ord, -1 * (ord - 1)};
		}

	public:
		coords_iterator() : ord{-1} {}
		coords_iterator(int r) : ord{r}, c{start(ord)}, dir{up} {}

		bool operator==(coords_iterator other) const {
			return (ord == -1 && other.ord == -1) ||
				(ord == other.ord && c == other.c);
		}

		bool operator!=(coords_iterator other) const {
			return !(*this == other);
		}

		coords_iterator& operator++() {
			coord t = c + dir;
			if (std::abs(t.x) > ord || std::abs(t.y) > ord) {
				turn();
				t = c + dir;
			}
			c = t;
			if (c == start(ord)) {
				ord = -1;
			}
			return *this;
		}

		coords_iterator operator++(int) {
			auto r = *this;
			++(*this);
			return r;
		}

		coord operator*() const {
			return c;
		}

	private:
		void turn() {
			if (dir == up) {
				dir = left;
			} else if(dir == left) {
				dir = bottom;
			} else if (dir == bottom) {
				dir = right;
			} else if (dir == right) {
				dir = up;
			}
		}

		int ord;
		coord c;
		coord dir;
};

class ring {
	public:
		ring(int r, int v=0) : ord{r} {
			coord c{ord, ord};
			while (c.x >= -ord) {
				values[c] = v;
				c.y -= 1;
				if (c.y < -ord) {
					c.x -= 1;
					c.y = ord;
				}
			}
		}

		coords_iterator begin() const {
			return coords_iterator(ord);
		}

		coords_iterator end() const {
			return coords_iterator();
		}


		int get(coord c) const {
			return values.at(c);
		}

		void set(coord c, int v) {
			values[c] = v;
		}

	private:
		int ord;
		std::unordered_map<coord, int> values;
};

int build_up_to(int key) {
	using namespace std;

	ring prev(0, 1);
	int ord = 1;
	while (true) {
		std::cout << "trying " << ord << "\n";
		ring r(ord);
		for (auto c : r) {
			int cell = 0;
			coord coords[8] = {
				{c.x - 1, c.y + 1},
				{c.x - 1, c.y + 0},
				{c.x - 1, c.y - 1},
				{c.x + 0, c.y + 1},
				{c.x + 0, c.y - 1},
				{c.x + 1, c.y + 1},
				{c.x + 1, c.y + 0},
				{c.x + 1, c.y - 1},
			};
			for (auto sub : coords) {
				if (abs(sub.x) > ord || abs(sub.y) > ord) {
					continue;
				}
				if (abs(sub.x) < ord && abs(sub.y) < ord) {
					cell += prev.get(sub);
				} else {
					cell += r.get(sub);
				}
			}
			if (cell >= key) {
				return cell;
			}
			r.set(c, cell);
		}
		prev = r;
		++ord;
	}
	return 0;
}

int main() {
	const int key = 325489;
	std::cout << build_up_to(key) << "\n";
}
