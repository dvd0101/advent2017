#include <algorithm>
#include <iostream>
#include <vector>
#include <unordered_map>

struct whitespace : std::ctype<char> {
    static const mask* make_table() {
        static std::vector<mask> v(classic_table(), classic_table() + table_size);
        v['p'] |= space;
        v['v'] |= space;
        v['a'] |= space;
        v[','] |= space;
        v['<'] |= space;
        v['>'] |= space;
        v['='] |= space;
        return &v[0];
    }
    whitespace(std::size_t refs = 0) : ctype(make_table(), false, refs) {}
};

struct vector {
	int64_t x, y, z;

	vector operator+(const vector& o) const {
		return {x + o.x, y + o.y, z + o.z};
	}

	bool operator==(const vector& o) const {
		return x == o.x && y == o.y && z == o.z;
	}

	bool operator<(const vector& o) const {
		return x < o.x && y < o.y && z < o.z;
	}
};

std::ostream& operator<<(std::ostream& out, const vector& v) {
	return out << "{" << v.x << "," << v.y << "," << v.z << "}";
}

template <class T>
inline void hash_combine(std::size_t& seed, const T& v) {
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

namespace std {
	template<> struct hash<::vector> {
        using argument_type = ::vector;
        using result_type = std::size_t;

        result_type operator()(argument_type const& s) const noexcept {
        	size_t seed = 0;
        	hash_combine(seed, s.x);
        	hash_combine(seed, s.y);
        	hash_combine(seed, s.z);
            return seed;
        }
    };
}

struct particle {
	vector pos;
	vector v;
	vector a;

	void run() {
		v = v + a;
		pos = pos + v;
	}

	int distance() const {
		return std::abs(pos.x) + std::abs(pos.y) + std::abs(pos.z);
	}
};

std::vector<particle> parse(std::istream& input) {
	using namespace std;
	std::vector<particle> output;

	input.imbue(locale(input.getloc(), new whitespace));
	int a, b, c, d, e, f, g, h, i;
	while (input >> a >> b >> c >> d >> e >> f >> g >> h >> i) {
		output.push_back({
			{a, b, c},
			{d, e, f},
			{g, h, i},
		});
	}
	return output;
}

int main() {
	auto particles = parse(std::cin);
	int counter = 10000;
	while (counter-- > 0) {
		std::unordered_map<vector, int> positions;
		for (auto& p : particles) {
			positions[p.pos] += 1;
		}
		auto it = std::remove_if(particles.begin(), particles.end(), [&](const auto& p) { return positions[p.pos] > 1; });
		particles.erase(it, particles.end());
		for (auto& p : particles) {
			p.run();
		}
	}
	std::cout << particles.size() << "\n";
}
