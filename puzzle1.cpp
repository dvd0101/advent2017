#include <iostream>

int main() {
	using namespace std;

	int sum = 0;
	char first = cin.get();
	char digit = first;
	for (char c = cin.get(); cin; c = cin.get()) {
		if (c == '\n' || c == '\r') {
			continue;
		}
		if (digit == c) {
			sum += digit - '0';
		}
		digit = c;
	}
	if (digit == first) {
		sum += digit - '0';
	}
	cout << sum << '\n';
}
