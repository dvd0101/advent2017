#include <iostream>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <vector>

struct component {
	int port1;
	int port2;

	bool operator==(component other) const {
		return port1 == other.port1 && port2 == other.port2;
	}

	bool compatible(int x) const {
		return port1 == x || port2 == x;
	}

	void make_compatible(int x) {
		if (port2 == x)
			std::swap(port1, port2);
	}
};

std::ostream& operator<<(std::ostream& out, const component& c) {
	return out << c.port1 << "/" << c.port2;
}

struct bridge {
	bridge(int s, int l) : strength{s}, length{l} {}
	bridge(component c) : strength{c.port1 + c.port2}, length{1} {}

	bridge operator+(const bridge& other) const {
		return {strength + other.strength, length + other.length};
	}

	int strength;
	int length;
};

std::vector<component> parse(std::istream& input) {
	std::vector<component> output;
	int p1, p2;
	while ((input >> p1).ignore(1) >> p2) {
		output.push_back({p1, p2});
	}
	return output;
}

std::vector<bridge> construct(int start, const std::vector<component>& components) {
	std::vector<bridge> output;
	for (auto c : components) {
		if (c.compatible(start)) {
			auto copy = components;
			copy.erase(std::remove(copy.begin(), copy.end(), c));
			c.make_compatible(start);
			auto children = construct(c.port2, copy);
			if (!children.empty()) {
				for (auto& b : children) {
					output.push_back(bridge(c) + b);
				}
			} else {
				output.push_back(bridge(c));
			}
		}
	}
	return output;
}

int main() {
	auto components = parse(std::cin);
	auto bridges = construct(0, components);
	std::sort(
		bridges.begin(),
		bridges.end(),
		[](const auto& a, const auto& b) {
			if (a.length == b.length)
				return a.strength < b.strength;
			return a.length < b.length;
		});

	std::cout << bridges.back().strength << "\n";
}
