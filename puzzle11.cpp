#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>

int main() {
	using namespace std;

	using memory_banks = vector<int>;
	vector<memory_banks> history;
	memory_banks memory{istream_iterator<int>(cin), istream_iterator<int>()};

	size_t counter = 0;
	while (find(begin(history), end(history), memory) == end(history)) {
		history.push_back(memory);
		int cells = 0;
		auto pos = max_element(begin(memory), end(memory));
		swap(cells, *pos);

		auto ix = distance(begin(memory), pos) + 1;
		while (cells--) {
			memory[ix++ % memory.size()]++;
		}
		counter++;
	}
	std::cout << counter << "\n";
}
