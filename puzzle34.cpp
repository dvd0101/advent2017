#include <iostream>
#include <vector>

int main() {
	int step;
	std::cin >> step;

	int pos = 0;
	int size = 1;
	int counter = 0;
	int p1 = 0;
	while (counter++ < 50000000) {
		pos = (pos + step) % size++;
		if (pos == 0) {
			p1 = counter;
		}
		pos++;
	}
	std::cout << p1 << "\n";
}
