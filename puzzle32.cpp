#include <cassert>
#include <iostream>
#include <regex>
#include <vector>
#include <variant>

struct csv_whitespace : std::ctype<char> {
    static const mask* make_table() {
        static std::vector<mask> v(classic_table(), classic_table() + table_size);
        v[','] |=  space;
        return &v[0];
    }
    csv_whitespace(std::size_t refs = 0) : ctype(make_table(), false, refs) {}
};


template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;


class spin {
	public:
		spin(int x) : count{x} {}

		void exec(std::string& prg) const {
			std::rotate(prg.rbegin(), prg.rbegin() + count, prg.rend());
		}
	private:
		int count;
};

class exch {
	public:
		exch(int x, int y) : a{x}, b{y} {}

		void exec(std::string& prg) const {
			std::swap(prg[a], prg[b]);
		}
	private:
		int a;
		int b;
};

class part {
	public:
		part(char x, char y) : a{x}, b{y} {}

		void exec(std::string& prg) const {
			std::swap(prg[prg.find(a)], prg[prg.find(b)]);
		}
	private:
		char a;
		char b;
};

using ins = std::variant<spin, exch, part>;

const std::regex parse_spin("s(\\d+)");
const std::regex parse_exch("x(\\d+)/(\\d+)");
const std::regex parse_part("p(\\w+)/(\\w+)");

ins parse(const std::string& cmd) {
	using namespace std;
	smatch m;
	switch (cmd[0]) {
		case 's':
			regex_match(begin(cmd), end(cmd), m, parse_spin);
			return spin(stoi(m[1]));
		case 'x':
			regex_match(begin(cmd), end(cmd), m, parse_exch);
			return exch(stoi(m[1]), stoi(m[2]));
		case 'p':
			regex_match(begin(cmd), end(cmd), m, parse_part);
			return part(m[1].str()[0], m[2].str()[0]);
		default:
			assert(0);
	}
}

void execute(std::string& prg, const std::vector<ins>& moves) {
	auto v = overloaded{
		[&](const auto& i) { i.exec(prg); }
	};
	for (auto& i: moves) {
		std::visit(v, i);
	}
}

int main() {
	using namespace std;
	string programs("abcdefghijklmnop");
	string original = programs;

	std::string cmd;
	vector<ins> moves;
	cin.imbue(locale(cin.getloc(), new csv_whitespace));
	while (cin >> cmd) {
		moves.push_back(parse(cmd));
	}

	int rep = 0;
	do {
		execute(programs, moves);
		rep++;
	} while (programs != original);
	int counter = 1000000000 % rep;
	while (counter-- > 0) {
		execute(programs, moves);
	}
	std::cout << programs << "\n";
}
