#include <iostream>
#include <functional>
#include <map>
#include <variant>
#include <vector>

using registers = std::map<char, int64_t>;
using memory = int[2];

class operand {
	public:
		operand(char c) : r{c} {}
		operand(int c) : r{'.'}, v{c} {}

		int operator()(registers& regs) const {
			return r != '.' ? regs[r] : v;
		}

	private:
		char r;
		int v;
};

struct snd {
	char reg;

	void execute(registers& regs, int&, memory m) const {
		m[0] = regs[reg];
	}
};

struct set {
	char reg;
	operand value;

	void execute(registers& regs, int&, memory) const {
		regs[reg] = value(regs);
	}
};

struct add {
	char reg;
	operand value;

	void execute(registers& regs, int&, memory) const {
		regs[reg] += value(regs);
	}
};

struct mul {
	char reg;
	operand value;

	void execute(registers& regs, int&, memory) const {
		regs[reg] *= value(regs);
	}
};

struct mod {
	char reg;
	operand value;

	void execute(registers& regs, int&, memory) const {
		regs[reg] = regs[reg] % value(regs);
	}
};

struct rcv {
	char reg;

	void execute(registers& regs, int&, memory m) const {
		if (regs[reg] != 0)
			m[1] = m[0];
	}
};

struct jgz {
	char reg;
	operand value;

	void execute(registers& regs, int& pc, memory) const {
		if (regs[reg] > 0)
			pc += value(regs);
	}
};


using ins = std::variant<snd, set, add, mul, mod, rcv, jgz>;

using one_op = std::map<std::string, std::function<ins(char)>>;
using two_op = std::map<std::string, std::function<ins(char, operand)>>;

one_op one_arg_op{
	{"snd", [](char c) { return snd{c}; }},
	{"rcv", [](char c) { return rcv{c}; }},
};

two_op two_args_op{
	{"set", [](char c, operand v) { return set{c, v}; }},
	{"add", [](char c, operand v) { return add{c, v}; }},
	{"mul", [](char c, operand v) { return mul{c, v}; }},
	{"mod", [](char c, operand v) { return mod{c, v}; }},
	{"jgz", [](char c, operand v) { return jgz{c, v}; }},
};

std::vector<ins> parse(std::istream& input) {
	std::vector<ins> output;

	std::string op;
	char r;
	std::string v;
	while(input >> op >> r) {
		if (one_arg_op.count(op)) {
			output.push_back(one_arg_op[op](r));
		} else {
			input >> v;
			auto& f = two_args_op.at(op);
			if (v[0] >= 'a' && v[0] <= 'z') {
				output.push_back(f(r, v[0]));
			} else {
				output.push_back(f(r, std::stoi(v)));
			}
		}
	}
	return output;
}

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

void execute(const ins& i, registers& regs, int& pc, memory m) {
	std::visit(overloaded{
		[&](auto& i) { i.execute(regs, pc, m); }
	}, i);
}

int execute(const std::vector<ins>& prg) {
	registers regs;
	int pc = 0;
	memory mem{0, 0};
	while (mem[1] == 0) {
		int s = pc;
		execute(prg[pc], regs, pc, mem);
		if (pc == s) pc++;
	}
	return mem[1];
}

int main() {
	auto prg = parse(std::cin);
	std::cout << execute(prg) << "\n";
}
