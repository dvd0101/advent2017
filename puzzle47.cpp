#include <iostream>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <vector>

struct component {
	int port1;
	int port2;

	bool operator==(component other) const {
		return port1 == other.port1 && port2 == other.port2;
	}

	bool compatible(int x) const {
		return port1 == x || port2 == x;
	}

	void make_compatible(int x) {
		if (port2 == x)
			std::swap(port1, port2);
	}
};

std::ostream& operator<<(std::ostream& out, const component& c) {
	return out << c.port1 << "/" << c.port2;
}

struct bridge {
	bridge(int s) : strength{s} {}
	bridge(component c) : strength{c.port1 + c.port2} {}

	bridge operator+(const bridge& other) const {
		return {strength + other.strength};
	}

	int strength;
};

std::vector<component> parse(std::istream& input) {
	std::vector<component> output;
	int p1, p2;
	while ((input >> p1).ignore(1) >> p2) {
		output.push_back({p1, p2});
	}
	return output;
}

std::vector<bridge> construct(int start, const std::vector<component>& components) {
	std::vector<bridge> output;
	for (auto c : components) {
		if (c.compatible(start)) {
			auto copy = components;
			copy.erase(std::remove(copy.begin(), copy.end(), c));
			c.make_compatible(start);
			auto children = construct(c.port2, copy);
			if (!children.empty()) {
				for (auto& b : children) {
					output.push_back(bridge(c) + b);
				}
			} else {
				output.push_back(bridge(c));
			}
		}
	}
	return output;
}

int main() {
	auto components = parse(std::cin);

	int s = 0;
	for (auto& b : construct(0, components)) {
		if (b.strength > s) s = b.strength;
	}
	std::cout << s << "\n";
}
