#include <iostream>
#include <bitset>
#include <regex>

const std::regex parser("\\d+");

template<size_t factor, size_t multiplier>
struct generator {
	generator(int v) : value{v} {};

	int operator()() {
		do {
			value = (value * factor) % 2147483647;
		} while (value % multiplier);
		return value;
	}

	int value;
};

int main() {
	std::string line;

	std::getline(std::cin, line);
	int startA = std::stoi(*std::sregex_token_iterator(line.begin(), line.end(), parser));
	std::getline(std::cin, line);
	int startB = std::stoi(*std::sregex_token_iterator(line.begin(), line.end(), parser));

	generator<16807, 4> g1(startA);
	generator<48271, 8> g2(startB);

	int counter = 0;
	int tests = 5000000;
	while (tests-- > 0) {
		if ((g1() & 65535) == (g2() & 65535)) {
			++counter;
		}
	}
	std::cout << counter << "\n";
}
