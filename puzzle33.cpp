#include <iostream>
#include <vector>

int main() {
	int step;
	std::cin >> step;

	std::vector<int> buffer = {0};
	int pos = 0;
	int counter = 0;
	while (counter++ < 2017) {
		pos = (pos + step) % buffer.size();
		buffer.insert(buffer.begin() + ++pos, counter);
	}
	std::cout << buffer[pos + 1] << "\n";
}
