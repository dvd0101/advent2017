#include <array>
#include <bitset>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

constexpr int elements = 256;
using dense_hash = std::array<uint8_t, elements / 16>;

dense_hash hash(std::vector<char> lengths) {
	lengths.push_back(17);
	lengths.push_back(31);
	lengths.push_back(73);
	lengths.push_back(47);
	lengths.push_back(23);

	using namespace std;
	std::array<uint8_t, elements> sparse;
	iota(begin(sparse), end(sparse), 0);

	int pos = 0;
	int skip = 0;
	int rounds = 64;
	while (rounds--) {
		for (auto l : lengths) {
			int swaps = l / 2;
			const int end = pos + l - 1;
			while (swaps > 0) {
				swap(
					sparse[(pos + swaps - 1) % elements],
					sparse[(end - (swaps - 1)) % elements]);
				swaps--;
			}
			pos += l + skip++;
		}
	}

	dense_hash dense;
	for (int block = 0; block < 16; ++block) {
		const auto start = &sparse[0] + block * 16;
		dense[block] = accumulate(
			next(start),
			start + 16,
			*start,
			[](char a, char b) { return a ^ b; });
	}

	return dense;
}

int main() {
	std::string key;
	std::cin >> key;

	int used = 0;
	for (int ix = 0; ix < 128; ++ix) {
		auto step = key + "-" + std::to_string(ix);
		std::vector<char> k{step.begin(), step.end()};
		auto h = hash(k);
		for (uint8_t octet : h) {
			used += std::bitset<8>(octet).count();
		}
	}
	std::cout << used << "\n";
}
