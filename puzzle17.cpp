#include <iostream>
#include <cassert>
#include <vector>

struct garbage {};

struct group {
	std::vector<group> children;
};

int sum(const group& root, int v=1) {
	int out = v;
	for (const auto& child : root.children) {
		out += sum(child, v+1);
	}
	return out;
}

void parse(std::istream& input, garbage) {
	char c;
	while (input >> c) {
		switch (c) {
			case '!':
				input.get();
				break;
			case '>':
				return;
		}
	}
}

void parse(std::istream& input, group& root) {
	char c;
	while (input >> c) {
		switch (c) {
			case '{':
				parse(input, root.children.emplace_back());
				break;
			case '<':
				parse(input, garbage{});
				break;
			case '}':
				return;
		}
	}
}

group parse(std::istream& input) {
	char c;
	input >> c;
	assert(c == '{');

	group root;
	parse(input, root);
	return root;
}

int main() {
	std::cout << sum(parse(std::cin)) << "\n";
}
