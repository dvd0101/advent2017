#include <array>
#include <bitset>
#include <iostream>
#include <numeric>
#include <queue>
#include <string>
#include <vector>

constexpr int elements = 256;
using dense_hash = std::array<uint8_t, elements / 16>;

dense_hash hash(std::vector<char> lengths) {
	lengths.push_back(17);
	lengths.push_back(31);
	lengths.push_back(73);
	lengths.push_back(47);
	lengths.push_back(23);

	using namespace std;
	std::array<uint8_t, elements> sparse;
	iota(begin(sparse), end(sparse), 0);

	int pos = 0;
	int skip = 0;
	int rounds = 64;
	while (rounds--) {
		for (auto l : lengths) {
			int swaps = l / 2;
			const int end = pos + l - 1;
			while (swaps > 0) {
				swap(
					sparse[(pos + swaps - 1) % elements],
					sparse[(end - (swaps - 1)) % elements]);
				swaps--;
			}
			pos += l + skip++;
		}
	}

	dense_hash dense;
	for (int block = 0; block < 16; ++block) {
		const auto start = &sparse[0] + block * 16;
		dense[block] = accumulate(
			next(start),
			start + 16,
			*start,
			[](char a, char b) { return a ^ b; });
	}

	return dense;
}

using disk_t = char[128][128];

void fill_region(disk_t& disk, int row, int col) {
	using point = std::pair<int, int>;
	std::queue<point> stack;
	stack.push({row, col});
	while(!stack.empty()) {
		point p = stack.front();
		stack.pop();

		auto [r, c] = p;
		disk[r][c] = 0;
		if (r > 0 && disk[r-1][c] == 1) {
			stack.push({r-1, c});
		}
		if (r < 127 && disk[r+1][c] == 1) {
			stack.push({r+1, c});
		}
		if (c > 0 && disk[r][c-1] == 1) {
			stack.push({r, c-1});
		}
		if (c < 127 && disk[r][c+1] == 1) {
			stack.push({r, c+1});
		}
	}
}

int regions_count(disk_t& disk) {
	int counter = 0;
	for (int row = 0; row < 128; ++row) {
		for (int col = 0; col < 128; ++col) {
			if (disk[row][col] == 1) {
				++counter;
				fill_region(disk, row, col);
			}
		}
	}
	return counter;
}

int main() {
	std::string key;
	std::cin >> key;


	disk_t disk;
	for (int row = 0; row < 128; ++row) {
		auto step = key + "-" + std::to_string(row);
		std::vector<char> k{step.begin(), step.end()};
		int col = 0;
		for (auto octet : hash(k)) {
			std::bitset<8> bits(octet);
			for (int ix = 7; ix >= 0; --ix) {
				disk[row][col++] = bits[ix];
			}
		}
	}

	for (int col = 0; col < 128; col++) {
		std::cout << +disk[0][col];
	}
	std::cout << "\n";
	std::cout << regions_count(disk) << "\n";
}
