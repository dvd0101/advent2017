#include <algorithm>
#include <cassert>
#include <iostream>
#include <string>
#include <vector>

using map = std::vector<std::vector<char>>;

map parse(std::istream& input) {
	map output;
	size_t cols = 0;
	for (std::string line; std::getline(input, line); ) {
		auto& row = output.emplace_back();
		std::transform(
			line.begin(),
			line.end(),
			std::back_inserter(row),
			[](char c) {
				if (c == '-' || c == '|' || c == '+')
					return static_cast<char>(1);
				if (c == ' ')
					return static_cast<char>(0);
				return c;
			});
		cols = std::max(cols, row.size());
    }

	for (auto& row : output)
		row.resize(cols);

    return output;
}

enum class dir {
	down,
	right,
	up,
	left,
	stop,
};

struct pos {
	int x, y;

	pos next(dir d) const {
		switch (d) {
			case dir::down:
				return {x, y+1};
			case dir::right:
				return {x+1, y};
			case dir::up:
				return {x, y-1};
			case dir::left:
				return {x-1, y};
			case dir::stop:
				return {x, y};
		}
	}
};

std::ostream& operator<<(std::ostream& out, pos p) {
	return out << p.x << "," << p.y;
}

int look(const map& m, pos p) {
	if (p.y < 0 || p.y >= m.size() || p.x < 0 || p.x >= m[0].size()) {
		return 0;
	}
	return m[p.y][p.x];
}

dir next_direction(const map& m, pos p, dir curr) {
	auto try_ = [&](dir d1, dir d2) {
		pos p1 = p.next(d1);
		if (look(m, p1)) return d1;
		pos p2 = p.next(d2);
		if (look(m, p2)) return d2;
		return dir::stop;
	};
	switch (curr) {
		case dir::up:
		case dir::down:
			return try_(dir::right, dir::left);
		case dir::right:
		case dir::left:
			return try_(dir::up, dir::down);
		default:
			break;
	};
	return dir::stop;
}

int run(const map& m) {
	pos p = {
		std::distance(m[0].begin(), std::find(m[0].begin(), m[0].end(), 1)),
		0
	};
	dir d = dir::down;
	int counter = 1;
	while (d != dir::stop) {
		pos next = p.next(d);
		char v = look(m, next);
		if (v == 0) {
			d = next_direction(m, p, d);
		} else {
			counter++;
			p = next;
		}
	}
	return counter;
}

int main() {
	std::cout << run(parse(std::cin)) << "\n";
}
