#include <iostream>
#include <regex>
#include <string>

std::regex word_splitter("\\w+");

bool is_valid(const std::string& s) {
	using namespace std;
	vector<string> words(
		sregex_token_iterator(begin(s), end(s), word_splitter),
		sregex_token_iterator());
	sort(begin(words), end(words));
	for (size_t ix = 0; ix < words.size() - 1; ix++) {
		if (words[ix] == words[ix + 1]) {
			return false;
		}
	}
	return true;
}

int main() {
	int r = 0;
	for(std::string line; std::getline(std::cin, line);) {
		if (is_valid(line)) {
			++r;
		}
	}
	std::cout << r << "\n";
}
