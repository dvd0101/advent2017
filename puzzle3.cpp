#include <iostream>
#include <sstream>
#include <limits>

int difference(std::string row) {
	std::istringstream in(row);
	int min = std::numeric_limits<int>::max();
	int max = std::numeric_limits<int>::min();
	int value;
	for (;;) {
		in >> value;
		if (!in) {
			break;
		}
		if (value < min) {
			min = value;
		}
		if (value > max) {
			max = value;
		}
	}
	return max - min;
}

int main() {
	using namespace std;

	int sum = 0;
	for (;;) {
		std::string row;
		getline(cin, row, '\n');
		if (!cin) {
			break;
		}
		sum += difference(row);
	}
	cout << sum << '\n';
}
