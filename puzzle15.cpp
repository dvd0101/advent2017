#include <cassert>
#include <iostream>
#include <regex>
#include <string>
#include <unordered_map>
#include <vector>

using registers = std::unordered_map<std::string, int>;

enum class condition {
	lt,
	lte,
	gt,
	gte,
	eq,
	neq
};

struct check {
	check(condition c, std::string r, int v) : cond{c}, reg{r}, value{v} {}

	condition cond;
	std::string reg;
	int value;

	bool execute(registers& regs) const {
		const int rv = regs[reg];
		switch (cond) {
			case condition::lt:
				return rv < value;
			case condition::lte:
				return rv <= value;
			case condition::gt:
				return rv > value;
			case condition::gte:
				return rv >= value;
			case condition::eq:
				return rv == value;
			case condition::neq:
				return rv != value;
		}
		return false;
	}
};

enum class opcode {
	inc,
	dec
};

struct operation {
	operation(opcode o, std::string r, int v) : op{o}, reg{r}, value{v} {}

	void execute(registers& regs) const {
		switch (op) {
			case opcode::inc:
				regs[reg] += value;
				break;
			case opcode::dec:
				regs[reg] -= value;
				break;
		}
	}

	opcode op;
	std::string reg;
	int value;
};

struct ins {
	operation o;
	check c;

	void execute(registers& regs) const {
		if (c.execute(regs)) {
			o.execute(regs);
		}
	}
};

using instructions = std::vector<ins>;

const std::regex parser("(\\w+) (inc|dec) ([-\\d]+) if (\\w+) (<|<=|>|>=|==|!=) ([-\\d]+)");

instructions parse(std::istream& input) {
	using namespace std;

	instructions output;

	for (std::string line; std::getline(input, line); ) {
		smatch match;
		bool r = regex_match(line, match, parser);
		assert(r);

		opcode op = match[2] == "inc" ? opcode::inc : opcode::dec;
		condition c;
		if (match[5] == "<") {
			c = condition::lt;
		} else if (match[5] == "<=") {
			c = condition::lte;
		} else if (match[5] == ">") {
			c = condition::gt;
		} else if (match[5] == ">=") {
			c = condition::gte;
		} else if (match[5] == "==") {
			c = condition::eq;
		} else if (match[5] == "!=") {
			c = condition::neq;
		} else {
			assert(0);
		}
		output.push_back(ins{
			operation(op, match[1], stoi(match[3])),
			check(c, match[4], stoi(match[6]))
		});
    }
    return output;
}

int main() {
	auto program = parse(std::cin);
	registers regs;
	for (auto& i : program) {
		i.execute(regs);
	}

	auto max = std::numeric_limits<int>::min();
	for (auto& [ reg, value ] : regs) {
		if (value > max) max = value;
	}
	std::cout << max << "\n";
}
