#include <iostream>
#include <sstream>
#include <vector>

int even_ratio(std::string row) {
	std::istringstream in(row);
	std::vector<int> numbers;
	int value;
	for (;;) {
		in >> value;
		if (!in) {
			break;
		}
		numbers.push_back(value);
	}
	for (size_t ix=0; ix < numbers.size() - 1; ix++) {
		const int val1 = numbers[ix];
		for (size_t iy=ix+1; iy < numbers.size(); iy++) {
			const int val2 = numbers[iy];
			if (val1 % val2 == 0) {
				return val1 / val2;
			} else if (val2 % val1 == 0) {
				return val2 / val1;
			}
		}
	}
	return 0;
}

int main() {
	using namespace std;

	int sum = 0;
	for (;;) {
		std::string row;
		getline(cin, row, '\n');
		if (!cin) {
			break;
		}
		sum += even_ratio(row);
	}
	cout << sum << '\n';
}
