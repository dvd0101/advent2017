#include <cassert>
#include <iostream>
#include <regex>
#include <vector>

struct csv_whitespace : std::ctype<char> {
    static const mask* make_table() {
        static std::vector<mask> v(classic_table(), classic_table() + table_size);
        v[','] |=  space;
        return &v[0];
    }
    csv_whitespace(std::size_t refs = 0) : ctype(make_table(), false, refs) {}
};

void spin(std::string& prg, int pos) {
	std::rotate(prg.rbegin(), prg.rbegin() + pos, prg.rend());
}

void exch(std::string& prg, int a, int b) {
	std::swap(prg[a], prg[b]);
}

void part(std::string& prg, std::string a, std::string b) {
	exch(prg, prg.find(a), prg.find(b));
}

const std::regex parse_spin("s(\\d+)");
const std::regex parse_exch("x(\\d+)/(\\d+)");
const std::regex parse_part("p(\\w+)/(\\w+)");

void execute(std::string& prg, const std::string& cmd) {
	using namespace std;
	smatch m;
	switch (cmd[0]) {
		case 's':
			regex_match(begin(cmd), end(cmd), m, parse_spin);
			spin(prg, stoi(m[1]));
			break;
		case 'x':
			regex_match(begin(cmd), end(cmd), m, parse_exch);
			exch(prg, stoi(m[1]), stoi(m[2]));
			break;
		case 'p':
			regex_match(begin(cmd), end(cmd), m, parse_part);
			part(prg, m[1], m[2]);
			break;
		default:
			assert(0);
	}
}

int main() {
	std::string programs("abcdefghijklmnop");

	std::string cmd;
	std::cin.imbue(std::locale(std::cin.getloc(), new csv_whitespace));
	while (std::cin >> cmd) {
		execute(programs, cmd);
	}
	std::cout << programs << "\n";
}
