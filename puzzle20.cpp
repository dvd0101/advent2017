#include <array>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <vector>

constexpr int elements = 256;
using dense_hash = std::array<uint8_t, elements / 16>;

dense_hash hash(const std::vector<char>& lengths) {
	using namespace std;
	std::array<uint8_t, elements> sparse;
	iota(begin(sparse), end(sparse), 0);

	int pos = 0;
	int skip = 0;
	int rounds = 64;
	while (rounds--) {
		for (auto l : lengths) {
			int swaps = l / 2;
			const int end = pos + l - 1;
			while (swaps > 0) {
				swap(
					sparse[(pos + swaps - 1) % elements],
					sparse[(end - (swaps - 1)) % elements]);
				swaps--;
			}
			pos += l + skip++;
		}
	}

	dense_hash dense;
	for (int block = 0; block < 16; ++block) {
		const auto start = &sparse[0] + block * 16;
		dense[block] = accumulate(
			next(start),
			start + 16,
			*start,
			[](char a, char b) { return a ^ b; });
	}

	return dense;
}

int main() {
	std::vector<char> lengths;
	char x;
	while (std::cin >> x) {
		lengths.push_back(x);
	}
	lengths.push_back(17);
	lengths.push_back(31);
	lengths.push_back(73);
	lengths.push_back(47);
	lengths.push_back(23);

	auto dense = hash(lengths);

	std::cout << std::hex << std::setfill('0');
	for (char block : dense) {
		std::cout << std::setw(2) << +static_cast<unsigned char>(block);
	}
}
