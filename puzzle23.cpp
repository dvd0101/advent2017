#include <cassert>
#include <iostream>
#include <queue>
#include <regex>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using links = std::unordered_map<int, std::vector<int>>;

const std::regex parser("(\\d+) <-> (.*)");
const std::regex endpoints("\\d+");

void parse(const std::string& line, links& output) {
	using namespace std;

	smatch splits;
	bool r = regex_match(begin(line), end(line), splits, parser);
	assert(r);

	vector<int> connections;

	std::string raw = splits[2];
	for_each(
		sregex_token_iterator(begin(raw), end(raw), endpoints),
		sregex_token_iterator(),
		[&connections](const std::string& endpoint) {
			connections.push_back(stoi(endpoint));
		});

	int a = stoi(splits[1]);
	output[a] = std::move(connections);
}

links parse(std::istream& input) {
	using namespace std;

	links output;
	for (string line; getline(input, line); ) {
		parse(line, output);
    }

    return output;
}


int group_size(links& conns, int id) {
	int size = 0;

	std::queue<int> stack;
	std::unordered_set<int> processed;

	stack.push(id);
	while (!stack.empty()) {
		const int src = stack.front();
		stack.pop();
		if (processed.count(src)) {
			continue;
		}
		processed.insert(src);
		++size;

		for (int dst : conns[src]) {
			stack.push(dst);
		}
	}

	return size;
}

int main() {
	links connections = parse(std::cin);
	std::cout << group_size(connections, 0) << "\n";
}
