#include <cassert>
#include <iostream>
#include <unordered_map>

struct coords {
	int x, y;

	bool operator==(coords c) const {
		return x == c.x && y == c.y;
	}

	coords& operator+=(coords c) {
		x += c.x;
		y += c.y;
		return *this;
	}
};

std::ostream& operator<<(std::ostream& out, coords c) {
	return out << "[" << c.x << "," << c.y << "]";
}

template <class T>
inline void hash_combine(std::size_t& seed, const T& v) {
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

namespace std {
	template<> struct hash<coords> {
        using argument_type = coords;
        using result_type = std::size_t;

        result_type operator()(argument_type const& s) const noexcept {
        	size_t seed = 0;
        	hash_combine(seed, s.x);
        	hash_combine(seed, s.y);
            return seed;
        }
    };
}

struct sparse_map {
	void infect(coords c) {
		map_[c] = 1;
	}

	void clear(coords c) {
		map_.erase(c);
	}

	bool test(coords c) const {
		return map_.count(c) == 1;
	}

	std::unordered_map<coords, bool> map_;
};

class carrier {
	public:
		carrier(coords p) : pos{p}, dir{0, 1} {}

		int infect(sparse_map& map) {
			int i;
			if (map.test(pos)) {
				right();
				map.clear(pos);
				i = 0;
			}
			else {
				left();
				map.infect(pos);
				i = 1;
			}
			pos += {dir.x, -dir.y};
			return i;
		}

	private:
		void left() {
			dir = coords{-dir.y, dir.x};
		}

		void right() {
			dir = coords{dir.y, -dir.x};
		}

	coords pos;
	coords dir;
};

std::pair<sparse_map, coords> parse(std::istream& input) {
	coords c{};
	sparse_map map;
	int width = 0;
	char ch;
	while (input.read(&ch, 1)) {
		switch (ch) {
			case '#':
				map.infect(c);
				[[fallthrough]];
			case '.':
				c.x++;
				break;
			case '\n':
				if (width == 0) {
					width = c.x;
				} else {
					assert(width == c.x);
				}
				c.y++;
				c.x = 0;
				break;
			default:
				assert(0);
		}
	}
	return {map, coords{width / 2, c.y / 2}};
}

int main() {
	auto [map, start] = parse(std::cin);

	int infections = 0;
	carrier virus(start);

	int steps = 10000;
	while (steps-- > 0) {
		infections += virus.infect(map);
	}

	std::cout << infections << "\n";
}
