#include <iostream>
#include <string>

int main() {
	using namespace std;

	string content;
	getline(cin, content, '\n');

	const int forward = content.size() / 2;
	int sum = 0;
	for (size_t ix = 0; ix < content.size(); ix++) {
		const char digit = content[ix];
		if (digit == content[(ix + forward) % content.size()]) {
			sum += digit - '0';
		}
	}
	cout << sum << '\n';
}
