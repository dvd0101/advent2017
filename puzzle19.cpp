#include <array>
#include <iostream>
#include <numeric>
#include <vector>

struct csv_whitespace : std::ctype<char> {
    static const mask* make_table() {
        // make a copy of the "C" locale table
        static std::vector<mask> v(classic_table(), classic_table() + table_size);
        v[','] |=  space;  // comma will be classified as whitespace
        return &v[0];
    }
    csv_whitespace(std::size_t refs = 0) : ctype(make_table(), false, refs) {}
};

constexpr int elements = 256;

int main() {
	using namespace std;
	array<uint8_t, elements> list;
	iota(begin(list), end(list), 0);

	vector<int> lengths;
	cin.imbue(locale(cin.getloc(), new csv_whitespace));
	int x;
	while (std::cin >> x) {
		lengths.push_back(x);
	}

	int pos = 0;
	int skip = 0;
	for (auto l : lengths) {
		int swaps = l / 2;
		const int end = pos + l - 1;
		while (swaps > 0) {
			swap(
				list[(pos + swaps - 1) % elements],
				list[(end - (swaps - 1)) % elements]);
			swaps--;
		}
		pos += l + skip++;
	}

	std::cout << list[0] * list[1] << "\n";
}
