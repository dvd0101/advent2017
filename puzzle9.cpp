#include <iostream>
#include <iterator>
#include <vector>

int main() {
	using namespace std;

	vector<int> program{istream_iterator<int>(cin), istream_iterator<int>()};
	int pc = 0;
	size_t counter = 0;
	while (pc >= 0 && pc < static_cast<int>(program.size())) {
		pc = pc + program[pc]++;
		counter++;
	}
	std::cout << counter << "\n";
}
